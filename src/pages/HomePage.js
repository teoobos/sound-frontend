import React from 'react';
//import MusicCard from '../components/MusicCard';
import MusicTable from '../components/MusicTable';
import {Container, Col, Row} from 'react-bootstrap';
import axios from 'axios';

export default class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            response: [] 
        }
    }

    
    componentDidMount() {
        //let response = []
        axios.get('http://192.168.1.2:5000/songs').then(res => {
            this.setState({response: res.data})
        });        
    }

    render() {
        return(
            <div>
                <Container>
                    <Row>
                        <Col>
                            <h1>Sound</h1>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <input/>
                        </Col>
                    </Row>
                    <Row style={{marginTop: '1%'}}>
                        <Col>
                            <MusicTable songs={this.state.response}/>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

