import React from 'react';
import {Card, Button} from 'react-bootstrap';

export default class MusicCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            play: false,
            id: 0,
            audio: Audio
        }
        this.playSong = this.playSong.bind(this);
        this.stopSong = this.stopSong.bind(this);
    }

    playSong(e){
        const id = e.target.id;

        if (this.state.play) {
            this.state.audio.pause();
        }
        this.updateSong(id);
        //let audio = new Audio('http://192.168.1.2:5000/songs/' + id);
        //this.setState({play: true, id: parseInt(id), audio: audio});
        //audio.play();
        //audio.onended = this.updateSong(parseInt(id) + 1)
    }

    updateSong(id) {
      let audio = new Audio('http://192.168.1.2:5000/songs/' + id);
      this.setState({play: true, id: parseInt(id), audio: audio});
      audio.play();
      console.log(this.state.audio.ended)
      id = parseInt(id) + 1

      //audio.addEventListener("ended", this.updateSong(id))
      audio.onended = () => {this.updateSong(id)};
    }

    stopSong(e) {
       this.state.audio.pause();
       this.setState({play: false});
    }

    isHidden(isPlayButton, cardId) {
        cardId = parseInt(cardId);
        if (isPlayButton) {
            if (this.state.play === false) {
                return false;
            }
            if (this.state.play === true && cardId !== this.state.id) {
                return false;
            }
            if (this.state.play === true && cardId === this.state.id) {
                return true;
            }
        }
        else {
            if (this.state.play === false) {
                return true;
            }
            if (this.state.play === true && cardId !== this.state.id) {
                return true;
            }
            if (this.state.play === true && cardId === this.state.id) {
                return false;
            }
        }
    }

    renderCards() {
        let cards = this.props.songs.map((el) => {
            return (
                <Card key={el.id}>
                    <Card.Body>
                        <Card.Text>{el.name}</Card.Text>
                        <Button variant="primary" id={el.id} onClick={this.playSong}  hidden={this.isHidden(true, el.id)}>Play</Button>
                        <Button variant="danger" id={el.id} onClick={this.stopSong} hidden={this.isHidden(false, el.id)}>Stop</Button>
                    </Card.Body>
                </Card>
            );
        });
        return(cards);
    }

    render() {
        return(
            <div>
               {this.renderCards()}
            </div>
        );
    }
}