import React from 'react';

import { Table, Button } from 'react-bootstrap';

export default class MusicTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      play: false,
      id: 0,
      audio: Audio
    }
    this.playSong = this.playSong.bind(this);
    this.stopSong = this.stopSong.bind(this);
  }

  playSong(e) {
    const id = e.target.id;

    if (this.state.play) {
      this.state.audio.pause();
    }
    this.updateSong(id);
    //let audio = new Audio('http://192.168.1.2:5000/songs/' + id);
    //this.setState({play: true, id: parseInt(id), audio: audio});
    //audio.play();
    //audio.onended = this.updateSong(parseInt(id) + 1)
  }

  updateSong(id) {
    let audio = new Audio('http://192.168.1.2:5000/songs/' + id);
    this.setState({ play: true, id: parseInt(id), audio: audio });
    audio.play();
    console.log(this.state.audio.ended)
    id = parseInt(id) + 1

    //audio.addEventListener("ended", this.updateSong(id))
    audio.onended = () => { this.updateSong(id) };
  }

  stopSong(e) {
    this.state.audio.pause();
    this.setState({play: false});
 }

 isHidden(isPlayButton, cardId) {
     cardId = parseInt(cardId);
     if (isPlayButton) {
         if (this.state.play === false) {
             return false;
         }
         if (this.state.play === true && cardId !== this.state.id) {
             return false;
         }
         if (this.state.play === true && cardId === this.state.id) {
             return true;
         }
     }
     else {
         if (this.state.play === false) {
             return true;
         }
         if (this.state.play === true && cardId !== this.state.id) {
             return true;
         }
         if (this.state.play === true && cardId === this.state.id) {
             return false;
         }
     }
 }

  renderRows() {
    return this.props.songs.map((el) => {
      return (
        <tr>
          <td>
            <Button variant="primary" id={el.id} onClick={this.playSong} hidden={this.isHidden(true, el.id)}>Play</Button>
            <Button variant="danger" id={el.id} onClick={this.stopSong} hidden={this.isHidden(false, el.id)}>Stop</Button>
          </td>
          <td><span>{el.name}</span></td>
        </tr>
      )
    });

  }


  render() {
    return (
      <div>
        <Table responsive size="sm">
          <tbody>
            {this.renderRows()}
          </tbody>
        </Table>
      </div>
    );
  }
}